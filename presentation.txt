So today I am going to talk about how and why I use the following git sub
commands:

rebase, 
reflog, 
cherry-pick,
blame,
bisect,

and I am going to show you a cool flag -i or --interactive.


101 type refresher stuff here:
how to add / commit / branch
what is a commit
branching models --> out of scope!

rebase:
why would you rebase?
 - Local commits often have details that aren't necessary / for public
 consumption
 - removes the need to resolve the same dang merge conflict more than once
 - over time the git tree can get messy, leading to 'git hero'
 
 Rules of Thumb (that I ignore):
 never rebase a branch you've already pushed
 unless you own it
 and you are sure you are doing it for a good reason

 ex. when merging multiple development streams, you'll keep rebasing pull
 requests against the main branch to keep the tree tidy

 Common Problems:
 your branch and the remote have diverged!  You are two ahead and 2 behind
 Solutions: 
    
   //TODO: examples for each

   if remote is correct: git reset HEAD~2 --hard && git pull

   if yours is correct: git push -f  

   if both have work you'd like to keep:

   git rebase origin/branch-name
 

 

git reflog after git commit -am command:

git add present

probably going to get a cool merge conflict right here.  Delete this.  Don't
you hate these?

